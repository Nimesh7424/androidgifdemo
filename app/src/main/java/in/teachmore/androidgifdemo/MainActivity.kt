package `in`.teachmore.androidgifdemo

import `in`.teachmore.androidgifdemo.databinding.ActivityMainBinding
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        showGifVideo()
        startTimer()
    }

    private fun startTimer() {
        Handler().postDelayed({
            launchNextActivity()
        }, splashDelayTime())
    }

    private fun showGifVideo() {

        Glide.with(this)
            .asGif()
            .load("https://i.pinimg.com/originals/d5/44/ff/d544ffca4ecb461fc19da7e384cbc6d5.gif")
            .apply(RequestOptions.overrideOf(300,225))
            .into(binding.imageGif);

    }

    private fun splashDelayTime(): Long {
        return 3000
    }

    private fun launchNextActivity() {
        val mainScreenIntent = Intent(this, StartScreenActivity::class.java)
        this.startActivity(mainScreenIntent)
        finish()
    }
}