package `in`.teachmore.androidgifdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class StartScreenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start_screen)
    }
}